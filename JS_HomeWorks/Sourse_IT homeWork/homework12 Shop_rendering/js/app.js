
    'use strict';
    const products = [
        {
            "id": "6318be202dafb88857ced6ef",
            "price": 1758,
            "picture": '<img src="img/1.jpg"/>',
            "availableCount": 9,
            "name": "do enim",
            "description": "Sit ipsum nisi cillum eu excepteur dolore non nulla labore pariatur. Pariatur occaecat ad exercitation labore sint nulla aliqua id. Cupidatat commodo anim culpa cupidatat sint id. Dolore sit laboris minim consequat esse irure tempor commodo. Ex laborum minim qui adipisicing nulla magna et irure anim consectetur culpa fugiat mollit. Consequat cupidatat velit officia velit cillum cillum. Nulla duis Lorem irure dolor magna do incididunt labore pariatur.\r\n"
        },
        {
            "id": "6318be205b25750683803721",
            "price": 2436,
            "picture": '<img src="img/2.jpg"/>',
            "availableCount": 5,
            "name": "voluptate anim",
            "description": "Occaecat fugiat ut reprehenderit culpa minim voluptate. Enim occaecat ad reprehenderit officia irure enim. Ad nisi excepteur nulla laboris sit incididunt sit. Dolore commodo occaecat adipisicing aliqua eiusmod. Sint irure culpa dolore velit nostrud occaecat.\r\n"
        },
        {
            "id": "6318be20b958f314f55a2cfa",
            "price": 3189,
            "picture": '<img src="img/3.jpg"/>',
            "availableCount": 9,
            "name": "cillum eiusmod",
            "description": "Culpa eu enim sit cupidatat ipsum anim fugiat irure tempor incididunt pariatur ullamco amet. Ad magna incididunt reprehenderit eiusmod elit quis. Ipsum anim do ex sit voluptate adipisicing velit mollit enim magna cillum. Laborum dolore nulla esse cupidatat. Do laborum veniam id ut commodo quis duis enim elit mollit cillum. Commodo magna ea qui ipsum. Cupidatat id proident labore anim consectetur veniam laboris velit.\r\n"
        },
        {
            "id": "6318be20942c96af2a22fc30",
            "price": 2037,
            "picture": '<img src="img/4.jpg"/>',
            "availableCount": 3,
            "name": "fugiat Lorem",
            "description": "Laborum esse aute eu ullamco dolore. Eiusmod sit consectetur mollit ipsum incididunt aliquip ipsum consectetur qui ullamco velit. Aute qui anim qui adipisicing non. Duis exercitation consequat laborum ut nulla reprehenderit nisi cupidatat ex proident excepteur fugiat exercitation.\r\n"
        },
        {
            "id": "6318be20f78d4bc4b1118484",
            "price": 2571,
            "picture": '<img src="img/5.jpg"/>',
            "availableCount": 10,
            "name": "est irure",
            "description": "Elit laborum proident nostrud commodo minim. Labore ullamco irure anim exercitation dolor culpa duis labore reprehenderit non consectetur. Enim in reprehenderit amet non culpa eiusmod consequat ullamco. Lorem non laboris nostrud consequat nulla labore consectetur esse. Amet labore nostrud pariatur dolor do cillum eu mollit consequat exercitation proident dolor aliqua in. Anim et est est amet aute non. Ad do velit tempor elit et reprehenderit sint cillum cupidatat proident et consectetur.\r\n"
        },
        {
            "id": "6318be200d058394f96faa32",
            "price": 2461,
            "picture": '<img src="img/6.jpg"/>',
            "availableCount": 9,
            "name": "irure dolore",
            "description": "Irure culpa irure eiusmod officia quis ex aute aute elit adipisicing officia minim ullamco. Dolor sit aliqua est pariatur aliqua. Mollit anim proident voluptate laborum reprehenderit occaecat enim consequat qui laboris. Nulla nostrud officia veniam tempor laboris voluptate dolore voluptate magna. Laboris consequat magna eiusmod ea reprehenderit anim. Nisi deserunt laborum occaecat magna magna deserunt qui deserunt. Amet veniam magna est aliqua ex reprehenderit non ea amet commodo qui minim.\r\n"
        },
        {
            "id": "6318be204295dde104ae26a5",
            "price": 3523,
            "picture": '<img src="img/7.jpg"/>',
            "availableCount": 4,
            "name": "laborum est",
            "description": "Aliquip consectetur pariatur nostrud do fugiat laboris quis. Laboris dolore nulla consectetur reprehenderit ea officia incididunt. Eu adipisicing cupidatat id enim ex eiusmod dolore reprehenderit sint enim nulla deserunt ea minim. Aliqua quis et minim ex sint est aliqua laboris.\r\n"
        },
        {
            "id": "6318be200e26561d690d61a4",
            "price": 1130,
            "picture": '<img src="img/8.jpg"/>',
            "availableCount": 5,
            "name": "magna cupidatat",
            "description": "Qui enim adipisicing proident nostrud eiusmod nisi duis adipisicing officia excepteur. Esse qui veniam fugiat adipisicing do excepteur. Elit est incididunt pariatur aute voluptate anim non dolor voluptate incididunt. Ex incididunt laborum aliqua voluptate. Tempor magna proident proident consectetur dolor dolore ex. Aliqua laborum velit do nostrud magna ipsum elit dolore. Sint laborum magna dolore voluptate occaecat.\r\n"
        },
        {
            "id": "6318be20e6264ff0e640c5b2",
            "price": 1834,
            "picture": '<img src="img/9.jpg"/>',
            "availableCount": 4,
            "name": "laboris ut",
            "description": "Fugiat aliqua Lorem ad nulla sunt excepteur anim deserunt ullamco ex incididunt eu. Anim culpa sit sunt sit voluptate incididunt laboris deserunt deserunt culpa. Excepteur aliquip ut ipsum excepteur irure eiusmod. Ad deserunt esse magna voluptate consectetur aute exercitation tempor commodo aliqua. Velit ex non ex laboris culpa cupidatat nulla ipsum anim. Eu velit anim esse elit qui aliquip ex nulla mollit duis dolor dolor.\r\n"
        },
        {
            "id": "6318be208862533fb4735214",
            "price": 3435,
            "picture": '<img src="img/10.jpg"/>',
            "availableCount": 2,
            "name": "culpa ullamco",
            "description": "Pariatur commodo aute qui eiusmod labore adipisicing amet quis ad in occaecat reprehenderit nisi. Dolor commodo consectetur sit dolore officia et quis nulla aute nulla ut eiusmod Lorem in. Sint ipsum ullamco aliqua mollit ullamco culpa. Reprehenderit reprehenderit fugiat laboris velit commodo aute magna consequat. Laborum sit magna veniam in consequat tempor aliquip ipsum Lorem cupidatat nulla enim.\r\n"
        }
    ];

                      
        let templateCard = document.querySelector("#templateCard"); 
        let templateCart = document.querySelector("#templateCart");
        let LiProducts = document.querySelector("#prod");
        let buttonBuy = document.querySelector(".buy");
        let btQquantity = document.querySelector("#btQquantity")
        let totalSum = 0;
        
        
        //создаем каталог интернет-магазина 
        for(let i = 0; i < products.length; i++) {
            const prods = products[i];
            let CardClone = templateCard.content.cloneNode(true);
            CardClone.querySelector("#prices").textContent = prods.price;
            CardClone.querySelector("#nameCard").textContent = prods.name;
            CardClone.querySelector("#img").innerHTML = prods.picture;
            CardClone.querySelector("#idProdukt").textContent = prods.id
            document.querySelector("#prod").append(CardClone);
            
        }

		//создаем обработчик на клик и записывем id товарa в переменную
       

        LiProducts.addEventListener("click", function(e) {
            if (e.target.className != 'buy') {
                return
            }
            else {
            let buy = e.target.parentNode
            let buyId = buy.querySelector("#idProdukt").innerText
            addToCart(buyId)
            } }  
        )

        //создаем обработчик события для корзины и удаления товара
        cart.addEventListener("click", function(e) {
            if (e.target.className != "delete-content-cart") {
                return;
            } 
            else {
                totalSum -=  document.querySelector("#buyProductPrice").textContent;
                document.querySelector("#totalSum").textContent = totalSum;
                document.querySelector('.cart-content').remove();
            }
        })

         //создаем обработчик события для  корректировки количества товара в корзине
        

    // const shop = {
        
    //     cart: [],
    //     addToCart(productId) {
    //         // TODO: need to implement this method. Find product by id and add it to cart
            function addToCart(productId) {
                for(let i = 0; i < products.length; i++) {
                const cart = products[i];
                if(cart.id == productId) {
                totalSum += cart.price; 
                let quantityProdukt = 1;
                let CartClone = templateCart.content.cloneNode(true);
                CartClone.querySelector("#nameProduct").textContent = cart.name;
                CartClone.querySelector("#buyProductPrice").textContent = cart.price;
                CartClone.querySelector("#imgCart").innerHTML = cart.picture;
                CartClone.querySelector("img").classList.add("img-cart");
                document.querySelector("#cartHeader").after(CartClone);
                document.querySelector(".quanty-cart").textContent = quantityProdukt;
                }
                document.querySelector("#totalSum").textContent = totalSum;
               
            }}
        
    //     },
    //     removeFromCart(productId) {
    //         // TODO: need to implement this method. remove product from cart by id
           
    //     },
    //     increaseQuantity(productId) {
    //         // TODO: need to implement this method. increase product quantity in cart
    //     },
    //     decreaseQuantity(productId) {
    //         // TODO: need to implement this method. decrease product quantity in cart
    //     },
    //     getTotalCart() {
    //       // TODO: need to return total sum in cart
    //     },
    //     renderOneProduct(product) {
    //         // TODO: need to implement this method. render one product in catalog
    //     },
    //     renderOneCartProduct(cartProduct) {
    //         // TODO: need to implement this method. render one product in cart
    //     },
    //     renderTotalCart() {
    //         // TODO: need to implement this method. render total in cart
    //     },
    //     renderCatalog() {
    //         // TODO: need to implement this method. render all products. need to use renderOneProduct
    //     },
    //     renderCart() {
    //         // TODO: need to implement this method. render cart. need to use renderOneCartProduct and renderTotalCart
    //     }
    // }
