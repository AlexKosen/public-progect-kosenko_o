'use strict'
//Task 1
const a = +prompt('Введите число:');
if (a === 10) {
    console.log('Верно');
} else {
    console.log('Неверно')
};


//Task 2
const min = +prompt('Введите минуты:');
if (min > 0 && min <= 14) {
    alert('это первая четветь часа');
} else if (min > 14 && min <= 29) {
    alert('это вторая четветь часа');
} else if (min > 29 && min <=44) {
    alert ('это третья четветь часа');
} else if (min > 44 && min <= 59) {
    alert('это четвертая четветь часа')
} else {
    alert('Вы неправильно ввели минуты');
}




//Task 3
let a = +prompt('Введите число:');
if (a === 0) {
    alert('Верно');
}
    else {
        alert('неверно');
};

//Task 4
let a = +prompt('Введите число:');
if (a > 0) {
    alert('Верно');
}
    else {
        alert('неверно');
};

//Task 5
let a = +prompt('Введите число:');
if (a < 0) {
    alert('Верно');
} else {
        alert('неверно');
};

//Task 6
let a = +prompt('Введите число:');
alert(a >= 0 ? 'Верно' : 'неверно');

//Task 7
let a = +prompt('Введите число:');
alert(a <= 0 ? 'Верно' : 'Неверно');

//Task 8
let a = +prompt('Введите число:');
alert(a !== 0 ? 'Верно' : 'Неверно');


//Task 9
let a = prompt('Введите слово: "test"');
alert(a === 'test' ? 'Верно' : 'Неверно');

//Task 10
let a = +prompt('Введите число:');
alert(a === 1 ? 'Верно' : 'Неверно');

//Task 11
let test = false;
alert(test = true ? 'Верно'  : 'Неверно');

//Task 11
let test = false;
if (!test) {
    console.log('неверно')
} else {
    console.log('верно')
};


//Task 12
let test = true;
alert(test != true ? 'Неверно' : 'Верно');

//Task 13
let a = 4;
if (a > 0 && a < 5) {
    console.log('Верно')
} else {
    console.log('Неверно')
};

//Task 14
let a = -2;
if (a === 0 || a === 2) {
    console.log(a + 7);
} else {
    console.log(a / 10);
};

//Task 15
let a = 3;
let b = 5;
if (a <= 3 && b >= 5) {
    console.log(a + b);
} else {
    console.log(a - b);
};

//Task 16
let a = 1;
let b = 7;
if (a > 2 && a < 11 || b >= 6 && b < 14) {
    console.log('Верно');
} else {
    console.log('Неверно');
};


// Task 17 (короткая запись)
let day = +prompt('Введите дату:');
if (day >= 1 && day <= 10) {
    alert('1-я декада месяца');
} else if (day >= 11 && day <= 20) {
    alert('2-я декада месяца');
} else if (day >= 21 && day <= 31) {
    alert('3-я декада месяца');
} else {
    alert('Нет такой даты');
}


// Task 18 
let month = prompt('Введите номер календарного месяца:');
if (month >= 1 && month == 12) {
    console.log('зима');
} else if (month >= 3 && month <= 5) {
    console.log('весна');
} else if (month >= 3 && month <= 8) {
    console.log('лето');
} else if (month >= 3 && month <= 11) {
    console.log('осень');
} else {
    console.log('нет такого календарного месяца');
}
