import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/css/vendor/bootstrap.min.css'
import './assets/css/vendor/font-awesome.css'
import './assets/css/vendor/flaticon/flaticon.css'
import './assets/css/vendor/slick.css'
import './assets/css/vendor/slick-theme.css'
import './assets/css/vendor/jquery-ui.min.css'
import './assets/css/vendor/sal.css'
import './assets/css/vendor/magnific-popup.css'
import './assets/css/vendor/base.css'
import './assets/css/style.min.css'


const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
