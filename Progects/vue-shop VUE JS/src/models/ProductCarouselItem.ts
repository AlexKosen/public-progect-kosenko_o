export default interface CarouselItem {
    id: number
    image: string
    title: string
    price: number
    oldPrice: number
    discount: number
}